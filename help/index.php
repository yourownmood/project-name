abc123567
<!DOCTYPE html>
<html lang="en-US" xmlns:fb="http://www.facebook.com/2008/fbml">
    <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# login-to-hp: http://ogp.me/ns/fb/login-to-hp#">
        <title>Curabitur id consectetur purus | HungryPeople</title>
        <link href='http://fonts.googleapis.com/css?family=Arvo:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="robots" content="index,follow" />
        <meta name="description" content="Hungry People is een culinaire community voor het vinden en het delen van de lekkerste recepten met al je vrienden." />
        <meta name="keywords" content="recepten,koken,kook,recept,kookboek" />
        <meta name="Rating" content="General" />
        <meta name="revisit-after" content="7 days" />
		<meta name="robots" content="noindex,nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0" />
        
                
        
        <!--[if lt IE 9]>
            
<style type="text/css">
    #header-wrapper, .searchrecept, #topbar, #loading_next_message {
	/*background: #fff;*/
	filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='#ccffffff', endColorstr='#ccffffff');
    }
    #imgoverlay {
        filter: progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr='#99000000', endColorstr='#99000000');
    }
    .header, .content {
        width: 924px;
    }

    .posting img {
        width: 100%;
        height: 215px;
    }

    .receptimg img#big_image {
        width: 469px;
        height: 225px;
    }

    input#search {
        background-position: 3px 10px !important;
    }
    .single input#search {
        background-position: 3px 5px !important;
        padding-top: 10px !important;
    }
    
    #topbar {
        overflow: hidden;
    }
    
    #topbar .button {
        background: transparent !important;
    }
    
    #add_recipe input, #add_recipe textarea {
        padding: 10px 0 !important;
    }
    
    #wrap_ingredients .grams {
        margin-right: 2px !important;
    }
    
    .row .recipe_error span {
        left: 10px;
    }
</style>        <![endif]-->
 
        <link rel="alternate" type="application/rss+xml" title="HungryPeople &raquo; Curabitur id consectetur purus Comments Feed" href="http://dev.hungrypeople.com/blog/appetizers/curabitur-id-consectetur-purus/feed/" />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://dev.hungrypeople.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://dev.hungrypeople.com/wp-includes/wlwmanifest.xml" /> 
<link rel='prev' title='Cras convallis leo sit amet ante leo sit amet ante tempus malesuada eleifend' href='http://dev.hungrypeople.com/blog/desserts/cras-convallis-leo-sit-amet-ante/' />
<link rel='next' title='Nulla et eros sed felis adipiscing hendrerit' href='http://dev.hungrypeople.com/blog/desserts/nulla-et-eros-sed-felis-adipiscing-hendrerit/' />
<meta name="generator" content="WordPress 3.6.1" />
<link rel='canonical' href='http://dev.hungrypeople.com/blog/appetizers/curabitur-id-consectetur-purus/' />
<link rel='shortlink' href='http://dev.hungrypeople.com/?p=20133' />
        <script type="text/javascript">
            var LIVE_SEARCH_URL = 'http://dev.hungrypeople.com/live-search/',
                INFINITE_SCROLL_IMG = "http://dev.hungrypeople.com/wp-content/themes/hungry-people2/images/infinite_scroll_loading.gif",
                HOME_URL = "http://dev.hungrypeople.com";
        </script>
    	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
            <style type="text/css">
                .ie #add_recipe .image_container
                {
                    display: none !important;
                }
				.ie #upload_image {
					height: 30px !important;
					margin-top: 10px;
					background: #fff;
					visibility: visible !important;
					border: 1px solid #c0c3cc;
					box-shadow: inset 0 1px 1px rgba(192, 195, 204, 0.5);
					border-radius: 3px;
					width: 98%;
				}
				.ie #user_pic_wrap .edit_recipe_icon {
					display: none !important;
				}
				.ie #user_avatar {
					cursor: normal !important;
				}
				.ie #upload_avatar {
					cursor: auto;
					height: 20px;
					width: 100%;
					background: #fff;
					cursor: pointer;
					border: 1px solid #c0c3cc;
					box-shadow: inset 0 1px 1px rgba(192, 195, 204, 0.5);
					border-radius: 3px;
				}
				.ie #avatar_change {
					position: absolute;
					width: 100%;
					top: 100%;
					left: 0;
					margin-top: -20px;
					z-index: 99;
				}
            </style>
    </head>
    <body itemscope itemtype="http://schema.org/WebPage">
	        
        <div id="mask"><div id="popup-wrap"></div></div>
        <div id="wrapper">

            <!-- ****************** HEADER ***************** -->
            <div id="header-wrapper">
                <div class="header column">
                    <div id="logo">
                        <a href="http://dev.hungrypeople.com"><img src="http://dev.hungrypeople.com/wp-content/themes/hungry-people2/images/logo.png" alt=""/></a>
                    </div>
                    <div id="main_menu" class="topheader">
                                                
                        <div id="social">
                            <ul id="menu-header" class="topbar_menu"><li class="header-menu-item"><a href="http://dev.hungrypeople.com/">Recepten</a></li><li class="header-menu-item"><a href="http://dev.hungrypeople.com/blog/">Blog</a></li><li class="header-menu-item"><a href="http://dev.hungrypeople.com/about/">Over ons</a></li></ul>                                
                            <div class="mini_menu">
                                <div id="ddd" class="wrapper-dropdown-2" tabindex="1">
                                    <span></span>
                                    <div class="menu-header-container"><ul id="menu-header-1" class="dropdown"><li class="header-menu-item"><a href="http://dev.hungrypeople.com/">Recepten</a></li><li class="header-menu-item"><a href="http://dev.hungrypeople.com/blog/">Blog</a></li><li class="header-menu-item"><a href="http://dev.hungrypeople.com/about/">Over ons</a></li></ul></div>                                    <div class="clear"></div>
                                </div>
                            </div>
                            <a class="add_recipe" href="http://dev.hungrypeople.com/recept-toevoegen/">Recept toevoegen</a>                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                
                            </div>
            <!-- **************** END HEADER *************** -->
            
            
            
                            <div id="flogin">
					                    <div id="flogin_box">
                        <h4>Welkom bij HungryPeople, Wij zijn een social food network.</h4>
                        <h5>Deel je recepten op HungryPeople en steun daarmee een goed doel. <a href="http://dev.hungrypeople.com/about/">Lees meer</a></h5>
                        <div class="sep"></div>
                        <!-- WP-FB AutoConnect Button v2.3.9 -->
	<div class="wrap_fbLoginButton">
	<!--<img class="log_fb_img" src="http://dev.hungrypeople.com/wp-content/themes/hungry-people2/images/fb_login_btn_placeholder.png" alt="" >-->
    <span class="fbLoginButton">
    <script type="text/javascript">//<!--
    document.write('<fb:login-button scope="email,publish_stream,publish_actions,read_friendlists" v="2" size="xlarge" onlogin="jfb_js_login_callback();">Login via facebook</fb:login-button>');    //--></script>
    </span>
    </div>
    
                        </div>
                </div>
                        
            <!-- **************** END LOGIN BOX *************** -->


            <!-- **************** CONTENT *************** -->

            <div id="content-wrapper">
              	<div class="content ">  


        <div class="space30"></div>
        <div class="searchrecept single">
           <form action="http://dev.hungrypeople.com/blog/appetizers/" name="s" id="searchform" method="get" target="" >
                <input type="search" id="search" class="category_section" name="s" autocomplete="off" placeholder="Zoek op ons blog..." />
                <input type="hidden" name="cat" value="1">
                <input type="submit" value="go" style="display: none;">
           </form>
        </div>
		
<ul id="slider1">
    <li>
		<input type="hidden" class="recipe-id" value="20133" />

        <div class="space30"></div>
                <div class="receptcontent" itemscope itemtype="http://schema.org/Recipe">
            <div class="drag-this">
                <div class="receptimg">
                    <img src="http://dev.hungrypeople.com/wp-content/uploads/kip-met-citroen-en-tijm-469x323.jpg" class="attachment-recipe-big wp-post-image" alt="kip met citroen en tijm" id="big_image" itemprop="image" />                    <div id="imgoverlay" class="blog_title">
                        <h4 itemprop="name">Curabitur id consectetur purus</h4>
                        <a href="javascript: void(0);" class="bx-next-replacer"></a>
                        <a href="javascript: void(0);" class="bx-prev-replacer"></a>
                    </div>
                </div>

                <div class="wrap_blog">
                   <div class="blog_content">
                      <p>Cras sollicitudin odio id molestie commodo. Maecenas tempus lorem vel consequat viverra. Maecenas ligula arcu, vestibulum id dolor vel, molestie sodales sapien. Cras semper massa enim, ac tincidunt massa fermentum eu. Curabitur id consectetur purus, ac blandit erat.</p>
                   </div>
                   <div class="clear"></div>

                    <div class="bottom-facebook">
                      <div class="social_icons">
                          <div id="fb">
							  <div class="fb-like" data-href="http://dev.hungrypeople.com/blog/appetizers/curabitur-id-consectetur-purus/" data-send="false" data-layout="button_count" data-width="80" data-show-faces="false" data-font="arial"></div>
                          </div> <!-- fb -->								
                          <div id="tw">								
                            <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
                           <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>							
                          </div> <!-- tw -->	
                          <div id="pi">
                              <script type="text/javascript" src="http://assets.pinterest.com/js/pinit.js"></script>
                              <a class="PIN_1361973313411_pin_it_button PIN_1361973313411_pin_it_button_inline PIN_1361973313411_pin_it_beside PIN_1361973313411_hazClick" data-pin-log="button_pinit" data-pin-config="beside" target="_blank" href="http://pinterest.com/pin/create/button/?url=http://dev.hungrypeople.com/blog/appetizers/curabitur-id-consectetur-purus/&media=http://dev.hungrypeople.com/wp-content/uploads/kip-met-citroen-en-tijm.jpg&description=Curabitur id consectetur purus" class="pin-it-button" count-layout="horizontal">
                                  <img src="http://passets-lt.pinterest.com/images/about/buttons/pinterest-button.png" height="21" alt="Follow Me on Pinterest" />
                              </a>
                          </div> <!-- pi -->
                          <div class="clear"></div>	
                       </div>
                       <div class="clear"></div>
                       <div class="space15"></div>

                       <meta itemprop="interactionCount" content="UserComments:0" />
                    </div>
                </div>
                
                                <div id="related_recipes">
                    <h4 class="list_title">Zoeken</h4>
                    
                    <ul>
                        <li>
                                    <a href="http://dev.hungrypeople.com/blog/appetizers/proin-tellus-dolor-volutpat-eget-tincidunt-eget/"><img src="http://dev.hungrypeople.com/wp-content/uploads/Gegrilde-caprese-naan-pizza-469x323.jpg" class="attachment-recipe-big wp-post-image" alt="Gegrilde caprese naan pizza" id="big_image" itemprop="image" /></a>
                                    <span>De lekkerste varianten op de klassieke caprese</span>
                                </li><li>
                                    <a href="http://dev.hungrypeople.com/blog/appetizers/aliquam-vel-mollis-lacus/"><img src="http://dev.hungrypeople.com/wp-content/uploads/image5-469x323.jpg" class="attachment-recipe-big wp-post-image" alt="image" id="big_image" itemprop="image" /></a>
                                    <span>Aliquam vel mollis lacus</span>
                                </li><li>
                                    <a href="http://dev.hungrypeople.com/blog/appetizers/bijzondere-kunstwerken-van-vless/"><img src="http://dev.hungrypeople.com/wp-content/uploads/banaan-smoothie-469x323.jpg" class="attachment-recipe-big wp-post-image" alt="banaan smoothie" id="big_image" itemprop="image" /></a>
                                    <span>Bijzondere kunstwerken van vless</span>
                                </li>                    </ul>
                </div>
                            </div>
        </div>
        	</li>
        	
<li>
		<input type="hidden" class="recipe-id" value="20133" />

        <div class="space30"></div>
                <div class="receptcontent" itemscope itemtype="http://schema.org/Recipe">
            <div class="drag-this">
                <div class="receptimg">
                    <img src="http://dev.hungrypeople.com/wp-content/uploads/kip-met-citroen-en-tijm-469x323.jpg" class="attachment-recipe-big wp-post-image" alt="kip met citroen en tijm" id="big_image" itemprop="image" />                    <div id="imgoverlay" class="blog_title">
                        <h4 itemprop="name">Curabitur id consectetur puru2s</h4>
                        <a href="javascript: void(0);" class="bx-next-replacer"></a>
                        <a href="javascript: void(0);" class="bx-prev-replacer"></a>
                    </div>
                </div>

                <div class="wrap_blog">
                   <div class="blog_content">
                      <p>Cras sollicitudin odio id molestie commodo. Maecenas tempus lorem vel consequat viverra. Maecenas ligula arcu, vestibulum id dolor vel, molestie sodales sapien. Cras semper massa enim, ac tincidunt massa fermentum eu. Curabitur id consectetur purus, ac blandit erat.</p>
                   </div>
                   <div class="clear"></div>

                    <div class="bottom-facebook">
                      <div class="social_icons">
                          <div id="fb">
							  <div class="fb-like" data-href="http://dev.hungrypeople.com/blog/appetizers/curabitur-id-consectetur-purus/" data-send="false" data-layout="button_count" data-width="80" data-show-faces="false" data-font="arial"></div>
                          </div> <!-- fb -->								
                          <div id="tw">								
                            <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
                           <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>							
                          </div> <!-- tw -->	
                          <div id="pi">
                              <script type="text/javascript" src="http://assets.pinterest.com/js/pinit.js"></script>
                              <a class="PIN_1361973313411_pin_it_button PIN_1361973313411_pin_it_button_inline PIN_1361973313411_pin_it_beside PIN_1361973313411_hazClick" data-pin-log="button_pinit" data-pin-config="beside" target="_blank" href="http://pinterest.com/pin/create/button/?url=http://dev.hungrypeople.com/blog/appetizers/curabitur-id-consectetur-purus/&media=http://dev.hungrypeople.com/wp-content/uploads/kip-met-citroen-en-tijm.jpg&description=Curabitur id consectetur purus" class="pin-it-button" count-layout="horizontal">
                                  <img src="http://passets-lt.pinterest.com/images/about/buttons/pinterest-button.png" height="21" alt="Follow Me on Pinterest" />
                              </a>
                          </div> <!-- pi -->
                          <div class="clear"></div>	
                       </div>
                       <div class="clear"></div>
                       <div class="space15"></div>

                       <meta itemprop="interactionCount" content="UserComments:0" />
                    </div>
                </div>
                
                                <div id="related_recipes">
                    <h4 class="list_title">Zoeken</h4>
                    
                    <ul>
                        <li>
                                    <a href="http://dev.hungrypeople.com/blog/appetizers/proin-tellus-dolor-volutpat-eget-tincidunt-eget/"><img src="http://dev.hungrypeople.com/wp-content/uploads/Gegrilde-caprese-naan-pizza-469x323.jpg" class="attachment-recipe-big wp-post-image" alt="Gegrilde caprese naan pizza" id="big_image" itemprop="image" /></a>
                                    <span>De lekkerste varianten op de klassieke caprese</span>
                                </li><li>
                                    <a href="http://dev.hungrypeople.com/blog/appetizers/aliquam-vel-mollis-lacus/"><img src="http://dev.hungrypeople.com/wp-content/uploads/image5-469x323.jpg" class="attachment-recipe-big wp-post-image" alt="image" id="big_image" itemprop="image" /></a>
                                    <span>Aliquam vel mollis lacus</span>
                                </li><li>
                                    <a href="http://dev.hungrypeople.com/blog/appetizers/bijzondere-kunstwerken-van-vless/"><img src="http://dev.hungrypeople.com/wp-content/uploads/banaan-smoothie-469x323.jpg" class="attachment-recipe-big wp-post-image" alt="banaan smoothie" id="big_image" itemprop="image" /></a>
                                    <span>Bijzondere kunstwerken van vless</span>
                                </li>                    </ul>
                </div>
                            </div>
        </div>
        	</li>
        	
        	
</ul>
<a class="bx-prev" id="prev-slide">prev</a>
<a class="bx-next" id="next-slide">next</a>
                </div>
            </div>
            <div class="clear"></div>
            <!-- **************** END CONTENT *************** -->


            <!-- **************** FOOTER *************** -->
           	            <div id="warning_popup" style="display:none;">
                <div id="warning_content">
                <h3>Je moet ingelogd zijn om deze actie uit te voeren.</h3><!-- WP-FB AutoConnect Button v2.3.9 -->
	<div class="wrap_fbLoginButton">
	<!--<img class="log_fb_img" src="http://dev.hungrypeople.com/wp-content/themes/hungry-people2/images/fb_login_btn_placeholder.png" alt="" >-->
    <span class="fbLoginButton">
    <script type="text/javascript">//<!--
    document.write('<fb:login-button scope="email,publish_stream,publish_actions,read_friendlists" v="2" size="xlarge" onlogin="jfb_js_login_callback();">Login via facebook</fb:login-button>');    //--></script>
    </span>
    </div>
    
            
                </div>
            </div>
        </div>
        
<!-- WP-FB AutoConnect Init v2.3.9 (NEW API) -->
    <div id="fb-root"></div>
    <script type="text/javascript">//<!--
      window.fbAsyncInit = function()
      {
        FB.init({
            appId: '439101682818247', status: true, cookie: true, xfbml: true, oauth:true, channelUrl: 'http://dev.hungrypeople.com/wp-content/plugins/wp-fb-autoconnect/facebook-platform/channel.html' 
        });
                    
      };

      (function() {
        var e = document.createElement('script');
        e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
        e.async = true;
        document.getElementById('fb-root').appendChild(e);
      }());
    //--></script>
    
<!-- WP-FB AutoConnect Callback v2.3.9 -->
  
  <form id="wp-fb-ac-fm" name="jfb_js_login_callback_form" method="post" action="http://dev.hungrypeople.com/wp-content/plugins/wp-fb-autoconnect/_process_login.php" >
      <input type="hidden" name="redirectTo" value="/blog/appetizers/curabitur-id-consectetur-purus/" />
      <input type="hidden" id="autoconnect_nonce" name="autoconnect_nonce" value="3a318cd924" /><input type="hidden" name="_wp_http_referer" value="/blog/appetizers/curabitur-id-consectetur-purus/" />   
    </form>
<script type="text/javascript">//<!--
    function jfb_js_login_callback()
    {
    //Make sure the user logged in
    FB.getLoginStatus(function(response)
    {
      if (!response.authResponse)
      {
      return;
      }

      document.jfb_js_login_callback_form.submit();
    });
    }
    //--></script>
    <script type='text/javascript' src='http://dev.hungrypeople.com/wp-includes/js/jquery/jquery.js?ver=1.10.2'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/html5shiv.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/jquery.easing.1.3.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/jquery-ui.min.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/jquery.ui.touch-punch.min.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/jquery.hpslider.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/jquery.form.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/jscrollbar/jquery.mousewheel.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/jscrollbar/mwheelIntent.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/jscrollbar/jquery.jscrollpane.min.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/searchbox.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/jquery.rateit.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/jquery.placeholder.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/custom.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/modernizr.custom.79639.js?ver=3.6.1'></script>
<script type='text/javascript' src='http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/jquery.uniform.js?ver=3.6.1'></script>
        
	<script type="text/javascript">
		$(document).ready(function() {
			var add_state = true;
			var loading_text = '<div class="content415 centered" id="recepy-view"><div class="content_inner" style="text-align: center;"><h2 class="page-title" style="margin-top: 100px; float: none; color: #36aacf; text-shadow: 0 1px 1px #444;">Recept laden</h2><img src="http://dev.hungrypeople.com/wp-content/themes/hungry-people2/images/ajax-loader.gif" style="padding-bottom: 200px;" /></div></div>';
			$('#slider1').hpslider({
				startAt: 0,
				onLastNext: function(slideWidth, initTouch) {
					var url = 'http://dev.hungrypeople.com/load-recipe/?next_recipe=' + $('.hpactive .recipe-id').val();
					
					$.post(url, {}, function(data) {
						$('#slider1').append('<li style="position: absolute; left: ' + slideWidth + 'px; width: ' + slideWidth + 'px;">' + data + '</li>');
						$('.rateit').rateit();
                        FB.XFBML.parse();
                        twttr.widgets.load();
                        initTouch($('#slider1 li:last-child'));
					});
					// $('#slider1').append('<li style="position: absolute; left: ' + slideWidth + 'px; width: ' + slideWidth + 'px;">test</li>');
				},
				onLastPrev: function(slideWidth, initTouch) {
					var url = 'http://dev.hungrypeople.com/load-recipe/?prev_recipe=' + $('.hpactive .recipe-id').val();

					var popped = ('state' in window.history && window.history.state !== null), initialURL = location.href;

					$.post(url, {}, function(data) {
						$('#slider1').prepend('<li style="position: absolute; left: -' + slideWidth + 'px; width: ' + slideWidth + 'px;">' + data + '</li>');
						$('.rateit').rateit();
                        FB.XFBML.parse();
                        twttr.widgets.load();
                        initTouch($('#slider1 li:first-child'));
					}); 
					// $('#slider1').prepend('<li style="position: absolute; left: -' + slideWidth + 'px; width: ' + slideWidth + 'px;">test</li>');
				},
				onAfterNext: function() {
                    if (add_state) {
                        var old_link = window.location.href;
                        var old_title = document.title;
                        var this_link = jQuery('.hpactive .heart').attr('data-href');
                        var this_title = jQuery('.hpactive #imgoverlay h4').text() + ' | Hungry People';
                        if (history.pushState) {
                            document.title = this_title;
                            history.pushState({title: old_title, link: old_link, direction: 1}, this_title, this_link);
                        }
                    }
                    add_state = true;
				},
				onAfterPrev: function() {
                    if (add_state) {
                        var old_link = window.location.href;
                        var old_title = document.title;
                        var this_link = jQuery('.hpactive .heart').attr('data-href');
                        var this_title = jQuery('.hpactive #imgoverlay h4').text() + ' | Hungry People';
                        if (history.pushState) {
                            document.title = this_title;
                            history.pushState({title: old_title, link: old_link, direction: 2}, this_title, this_link);
                        }
                    }
                    add_state = true;
				}
			});
			
			$(window).bind('popstate', function(e) {
				var popped = ('state' in window.history && window.history.state !== null);

				if (!popped) {
					var old_link = window.location.href;
	                var old_title = document.title;
	                history.replaceState({title: old_title, link: old_link, direction: 1});
				} else {
					add_state = false;
					var state = e.originalEvent.state;
					document.title = state.title;
					if (state.direction == 1) {
						e.originalEvent.state.direction = 2;
						$('#prev-slide').click();
					} else {
						e.originalEvent.state.direction = 1;
						$('#next-slide').click();
					}
					console.log(History);
				}
			});
		});
	</script>
   
        
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-39413282-1']);
            _gaq.push(['_trackPageview']);

            (function() {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>
		
		<script type="text/javascript" src="http://dev.hungrypeople.com/wp-content/themes/hungry-people2/js/jquery.placeholder.js"></script>
        
    </body>
</html>
